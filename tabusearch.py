import numpy as np
import random
import pandas as pd

def distance_matrix_maker(city):
    d =[]
    for i,kota in enumerate(city):
        less =city[i+1:]
        for j in less:
            d_temp = kota+j
            d.append(d_temp)
    value = np.random.randint(20,60,36)
    tenure= np.zeros(36).astype(int)
    long_mm=np.zeros(36).astype(int)
    d = {'distance': d, 'value': value,'tenure': tenure,'long_mm':long_mm}
    df = pd.DataFrame(data=d)
    return df

def initial_move(city):
    move = city.copy()
    random.shuffle(move)
    return move

def objective_value(df,initial_move):
    dv = []
    for i,city in enumerate(initial_move):
        if i == len(initial_move)-1:
            cm1 = city+initial_move[0]
            if distance['distance'].isin([cm1]).any():
                x = (int(distance[distance.distance==str(cm1)].value.values))
            else:
                cm2 = initial_move[0]+city
                x = (int(distance[distance.distance==str(cm2)].value.values))
            dv.append(x)
            break
        cm1 = city+initial_move[i+1]
        if distance['distance'].isin([cm1]).any():
            x = (int(distance[distance.distance==str(cm1)].value.values))
        else:
            cm2 = initial_move[i+1]+city
            x = (int(distance[distance.distance==str(cm2)].value.values))
        dv.append(x)
    obj_value = np.array(dv).sum()
    return obj_value

def swap_move(before,dist_matx,rand_mv):
    swap_idx = gen_random_nonrepet(rand_mv)
    obj_val = []
    sw = []
    sw_m =[]
    for e in (swap_idx):
        sw_move = before.copy()
        sw.append([before[e[1]],before[e[0]]])
        sw_move[e[0]] = before[e[1]]
        sw_move[e[1]] = before[e[0]]
        obj = objective_value(distance,sw_move)
        sw_m.append(sw_move)
        obj_val.append(obj)
    obj_mtx = np.array(obj_val)
    return [sw,obj_mtx,sw_m]

def swap_str(x):
    a = list(x)
    return a[1]+a[0]

def update_tenure(obj_val_new,obj_val_old,aspi_criteria,chos):    
    if distance['distance'].isin([chos]).any():
        tenure_before = distance[distance.distance==str(chos)].tenure.values[0]
        if tenure_before != 0: 
            if obj_val_new - obj_val_old > aspi_criteria:
                distance.loc[distance.distance == chos, 'tenure'] = 4
                distance.at[distance.distance == chos,'long_mm']= distance.loc[distance.distance == chos].filter(['long_mm']).apply(update_longterm,0)
                return True
            else:
                return False
        else:
            distance.loc[distance.distance == chos, 'tenure'] = 4
            distance.at[distance.distance == chos,'long_mm']= distance.loc[distance.distance == chos].filter(['long_mm']).apply(update_longterm,0)
            return True
        #check tenure, if 0, fill, if not, compare using aspiration criteria
    else:
        chos = swap_str(chos)
        #check tenure, if 0, fill, if not, compare using aspiration criteria
        tenure_before = distance[distance.distance==str(chos)].tenure.values[0]
        if tenure_before != 0: 
            if obj_val_new - obj_val_old > aspi_criteria:
                distance.loc[distance.distance == chos, 'tenure'] = 4
                distance.at[distance.distance == chos,'long_mm']= distance.loc[distance.distance == chos].filter(['long_mm']).apply(update_longterm,0)
                return True
            else:
                return False
        else:
            distance.loc[distance.distance == chos, 'tenure'] = 4
            distance.at[distance.distance == chos,'long_mm']= distance.loc[distance.distance == chos].filter(['long_mm']).apply(update_longterm,0)
            return True
        
def update(row):
    if row.tenure>0:
        return row.tenure-1
    else:
        return row
def update_longterm(row):
    return row +1

def gen_random_nonrepet(swap_count):
    rand = [np.random.choice(5, 2,replace=False)]
    for i in range (0,swap_count-1):
        idx = np.random.choice(5, 2,replace=False)
        rand = np.append(rand,[idx],axis=0)
    return rand